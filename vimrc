" search and highlight words under cursor
autocmd CursorMoved * exe printf('match IncSearch /\V\<%s\>/', escape(expand('<cword>'), '/\'))

" highlight search
set hlsearch

" enable mouse click to move caret and select with mouse
set mouse=a
" https://superuser.com/questions/146768/mouse-cursor-in-terminal

set smartindent

set number

set cursorline

highlight CursorLine cterm=NONE ctermfg=white ctermbg=232

" Show non-printable characters
" set listchars=eol:$,tab:>-,extends:>,precedes:<
" set list " Shows tabs and end of line
" http://vim.wikia.com/wiki/Highlight_unwanted_spaces
" for another non-keyboard chars https://unicode-table.com
" but not all terms can display all of unicode,
" also over ssh you can see not all chars
